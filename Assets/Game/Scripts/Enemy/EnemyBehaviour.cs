using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// hard surface collision - throwing ball at a wall or for example car accident
// trigger collision - illusion that you have touched something for example collecting coins
 
// null reference exception - object reference not set to an instance of an object // you are trying to access component that does not exist!

// has exit time - wait until previpus anim has finished

public class EnemyBehaviour : MonoBehaviour
{
    [SerializeField] private float _enemySpeed = 4.0f;
    [SerializeField] private float _maxXPos = 9.0f;
    [SerializeField] private float _minYUpPos = 4.5f;
    [SerializeField] private float _maxYPos = 5.5f;

    private AudioSource _explosionSound;
    private BoxCollider2D _objectCollider;

    private float screenBottom = -5.5f;

    private Vector3 _spawnPos;

    private string _playerName = "Player";
    private string _deathTrigger = "OnEnemyDeath";

    private PlayerMovement _player;

    private Animator _anim;
    
    private void RemoveCollider()
    {
        Destroy(_objectCollider); // removes collision for player not taking damage during the anim
    }

    private void Spawn()
    {
        float randomX = Random.Range(-_maxXPos, _maxXPos);
        _spawnPos = new Vector3(randomX, Random.Range(_minYUpPos, _maxYPos), 0); // random.range generates random number from two numbers 
        transform.position = _spawnPos; // set position 
    }
    private void Movement()
    {
        transform.Translate(Vector3.down * _enemySpeed * Time.deltaTime);

        // if position.y < bottom of the screen 
        // a new random x position 
        if (transform.position.y < screenBottom)
        {
            Spawn();
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.GetComponent<PlayerMovement>() != null && other.GetComponent<EnemyBehaviour>() == null) // damage the player
        {
            PlayerMovement playerMovement = other.GetComponent<PlayerMovement>();
            if (playerMovement != null) // null checking - get in habit of doing - helps in avoiding errors
            {
                // other.GetComponent<PlayerMovement>().Damage(); // calling method from access getcomponent to script and the method same can be done with meshrenderer.color for example
                playerMovement.Damage();
            }
            RemoveCollider();
            // _objectCollider.isTrigger = false; // disabling object collision for not taking damage during the anim
            _anim.SetTrigger(_deathTrigger);
            _enemySpeed = 0; // stop while dying
            _explosionSound.Play();
            Destroy(this.gameObject, _explosionSound.clip.length);
        }

        if (other.GetComponent<LaserBehaviour>()!=null) // damage enemy
        {
            Destroy(other.gameObject);
            _player.ScoreIncrease();
            _anim.SetTrigger(_deathTrigger);
            _enemySpeed = 0; // stop while dying 
            _explosionSound.Play();
            RemoveCollider();
            //_objectCollider.isTrigger = false; // disabling object collision for not taking damage during the anim
            Destroy(this.gameObject, _explosionSound.clip.length); // destroy after both anim and sound ended
        }
    }
    private void Awake()
    {
        _anim = this.gameObject.GetComponent<Animator>();
        _explosionSound = this.gameObject.GetComponent<AudioSource>();
        _objectCollider = this.gameObject.GetComponent<BoxCollider2D>();
    }
    private void Start()
    {
        Spawn();
        _player = GameObject.Find(_playerName).GetComponent<PlayerMovement>();
    }
    private void Update()
    {
        Movement();
    }
}
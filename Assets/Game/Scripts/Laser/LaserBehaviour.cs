using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// At the end of the day, software engineers and game developers are just professional googlers

public class LaserBehaviour : MonoBehaviour // Pseudo Code is Cool it is very helpful!
{

    [SerializeField] private float _flySpeed = 8.0f;
    private float _maxLaserYPos = 8.0f;

    private void FlyBehaviour()
    {
        // current position transform.translate go on vector.up * speed * time.deltatime
        transform.Translate(Vector3.up * _flySpeed * Time.deltaTime);
        // if transform.position.y >=8 
        // destroy the object
        if(this.transform.position.y >= _maxLaserYPos)
        {
            if(transform.parent != null) // if game object has a parent
            {
                Destroy(transform.parent.gameObject); // Destroy parent
            }
            Destroy(this.gameObject); // destroy this instance
        }
    }

    private void Update()
    {
        FlyBehaviour();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    [SerializeField] private Button _playButton = null;
    [SerializeField] private Button _quitButton = null;

    private int _gameScene = 1;

    private void PlayGame()
    {
        SceneManager.LoadScene(_gameScene);
    }
    private void QuitGame()
    {
        Application.Quit();
    }
    private void Awake()
    {
        _playButton.onClick.AddListener(PlayGame);
        _quitButton.onClick.AddListener(QuitGame);
    }
}

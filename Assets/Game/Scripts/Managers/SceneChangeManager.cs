using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChangeManager : MonoBehaviour
{
    [SerializeField] private GameObject _restartText = null;

    private int _gameSceneIndex = 1;

    private void OnRestartDemand()
    {
        SceneManager.LoadScene(_gameSceneIndex); // load current game scene
    }
    private void Update()
    {
        if(_restartText.gameObject.activeInHierarchy == true & Input.GetKeyDown(KeyCode.R)) // active in the hierarchy - checks if object is active in the hierarchy
        {
            OnRestartDemand();
        }
    }
}

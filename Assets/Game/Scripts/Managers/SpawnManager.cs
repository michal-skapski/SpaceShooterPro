using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [SerializeField] private float _respawnTime = 5.0f;
    [SerializeField] private GameObject _enemyPrefab = null;
    [SerializeField] private GameObject _enemyContainer = null;
    [SerializeField] private GameObject[] _powerUps = null;

    private bool _stopSpawning = false;

    // private IEnumerator _spawnEnemyRoutine; // you can make such variables!
    // private IEnumerator _spawnTripleShootBoostRoutine;

    private float _minTime = 3.0f;
    private float _maxTime = 8.0f;

    private float _minXGameArea = -7.0f;
    private float _maxXGameArea = 7.0f;
    private float _maxYGameArea = 5.0f;

    private int _amountOfPowerUpsArraySize = 3;

    private const int _startDelay = 2;

    public void StartSpawning()
    {
        StartCoroutine(SpawnEnemyRoutine());
        StartCoroutine(Booster());
    }

    private IEnumerator SpawnEnemyRoutine()
    {
        // while loop (infinite loop)
        // instantiate enemy prefab
        // yield wait for 5 seconds 
        yield return new WaitForSeconds(_startDelay);
        while (_stopSpawning == false)
        {
            // then this line is called
            GameObject newEnemy = Instantiate(_enemyPrefab); // assign temporary variable to the new created object
            newEnemy.transform.parent = _enemyContainer.transform; // this will make the enemy child of enemyContainer
            yield return new WaitForSeconds(_respawnTime); // wait for 5 seconds
        }
    }
    private IEnumerator Booster()
    {
        yield return new WaitForSeconds(_startDelay);
        while (_stopSpawning == false)
        {
            // every 3-7 seconds, spawn a power up
            float randomTime = Random.Range(_minTime, _maxTime);
            yield return new WaitForSeconds(randomTime);
            Vector3 posToSpawn = new Vector3(Random.Range(_minXGameArea, _maxXGameArea), _maxYGameArea, 0f);
            int randomPowerUp = Random.Range(0, _amountOfPowerUpsArraySize);
            Instantiate( _powerUps[randomPowerUp],posToSpawn, Quaternion.identity);
        }
    }
    public void OnPlayerDeath() // it's not good practice to change variables from the other script better to use method
    {
        _stopSpawning = true;
    }
}
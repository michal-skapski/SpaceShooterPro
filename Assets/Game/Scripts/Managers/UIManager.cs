using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    // none functionalities no teleporting, no bow shooting are existing unitil you create it!
    // ammo is just a variable!

    // when making UI make sure to do two things:

    // checked - preserve aspect ratio on your images - that will fix bug with pictures looking stretched

    // choose "Scale With Screen Size" on your canvas - that will made sure that UI gonna scale well on diffrent resolutions

    [SerializeField] private TextMeshProUGUI _scoreText = null;

    [SerializeField] private Image _liveDisplayImage = null;

    [SerializeField] private Sprite[] _livesSprites = null;

    [SerializeField] private TextMeshProUGUI _gameOverText = null;

    [SerializeField] private GameObject _restartText = null;

    private float _gameOverAnimationTime = 1f;

    private const string _scoreString = "Score: ";

    private int currentScore = 0;

    private string _gameOverNormal = "GAME OVER";
    private string _gameOverAnimated = "";

    public void IncreaseScore(int p_increaseAmount)
    {
        currentScore += p_increaseAmount;
        _scoreText.text = _scoreString + currentScore;
    }
    public void UpdateLives(int p_amountOfLives)
    {
        if (p_amountOfLives <= 0)
        {
            _liveDisplayImage.sprite = _livesSprites[0];
        }
        _liveDisplayImage.sprite = _livesSprites[p_amountOfLives];
    }
    public void OnPlayerDeath()
    {
        _gameOverText.gameObject.SetActive(true); // game over text active when player dies
        _restartText.gameObject.SetActive(true);
        StartCoroutine(GameOverFlickerRoutine()); // animation text
    }
    private IEnumerator GameOverFlickerRoutine()
    {
        while (true)
        {
            _gameOverText.text = _gameOverNormal;
            yield return new WaitForSeconds(_gameOverAnimationTime);
            _gameOverText.text = _gameOverAnimated;
            yield return new WaitForSeconds(_gameOverAnimationTime);
        }
    }
    private void Start()
    {
        _scoreText.text = _scoreString + currentScore;
    }
}
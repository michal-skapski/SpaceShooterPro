using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidBehaviour : MonoBehaviour
{
    [SerializeField] private SpawnManager _spawnManager = null;
    [SerializeField] private GameObject _explosionAnim = null;
    private AudioSource _explosionSound;
    private CircleCollider2D _objectCollider;
    private SpriteRenderer _spriteRenderer;
    private const float _rotateSpeed = 20f;
    private const float _animDuration = 2.5f;
    private Vector3 _rotateDirection = new Vector3(0, 0, -3);
    private void ConstantRotate()
    {
        this.transform.Rotate((_rotateDirection) * _rotateSpeed * Time.deltaTime); // constant rotate the object
    }
    // check for laser collision (trigger)
    // instantiate explosion at the position of the asteroid (us)
    // destroy the collision after 3 seconds
    private IEnumerator AsteroidDestroy()
    {
        _explosionSound.Play();
        yield return new WaitForSeconds(_animDuration);
        _spawnManager.StartSpawning(); // launch the spawning
        Destroy(this.gameObject, _explosionSound.clip.length);
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<LaserBehaviour>())
        {
            _objectCollider.isTrigger = false; // disabling object trigger for not taking damage any more
            _objectCollider.enabled = false; // disabling collider
            GameObject newExplosion = Instantiate(_explosionAnim, this.transform.position, Quaternion.identity);
            newExplosion.transform.parent = this.transform; // this will make him child of the asteroid
            Destroy(other.gameObject); // destroy the laser
            _spriteRenderer.enabled = false; // disabling the view of the asteroid
            StartCoroutine(AsteroidDestroy());            
        }
    }
    private void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _explosionSound = GetComponent<AudioSource>();
        _objectCollider = GetComponent<CircleCollider2D>();
    }
    private void Update()
    {
        ConstantRotate();
    }
}
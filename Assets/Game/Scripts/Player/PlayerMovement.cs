using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// learn pseudo code typing and get in habit to comment
// unity is component based design
// learn how to read documentation and how to research information
// don't copy and paste your code
// there is no bad way to write code there is a more optimal way however there is no wrong way and there are plenty ways to solve the problem

// always type pseudo code!

public class PlayerMovement : MonoBehaviour
{

    [SerializeField] private UIManager _uiManager = null;

    [SerializeField] private GameObject _shieldEffect = null;

    [SerializeField] private GameObject _leftEngineDamaged = null, _rightEngineDamaged = null;

    [SerializeField] private SpawnManager _spawnManager = null;
    [SerializeField] private int _lives = 3;

    private Vector3 _zeroPos = new Vector3(0, 0, 0); // all thre dimensia

    [SerializeField] private float _playerSpeed = 5f;
    [SerializeField] private float _boostedSpeed = 8.5f;

    [SerializeField] private float _boostedSpeedTime = 5;

    private float _normalSpeed;

    private string _horizontalAxis = "Horizontal";
    private string _verticalAxis = "Vertical";

    private float _horizontalInput;
    private float _verticalInput;

    private float _minYVal = -3.8f;
    private float _maxYVal = 2f;
    private float _maxXVal = 11f;

    private bool _isShieldActive = false;

    private int _pointsForKll = 10;

    private const int _twoLives = 2;
    private const int _oneLive = 1;

    public void OnSpeedBoostCollected()
    {
        _playerSpeed = _boostedSpeed;
        StartCoroutine(SpeedBoostDown());
    }
    public void OnShieldCollected()
    {
        _isShieldActive = true;
        _shieldEffect.gameObject.SetActive(true);
    }
    private IEnumerator SpeedBoostDown()
    {
        yield return new WaitForSeconds(_boostedSpeedTime);
        _playerSpeed = _normalSpeed;
    }

    private void PositionRestrain()
    {
        transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y, _minYVal, _maxYVal), 0); // setting minimum and max position on the Y axis position of our player
        if (transform.position.x > _maxXVal)
        {
            transform.position = new Vector3(-_maxXVal, transform.position.y, 0f);
        }
        if (transform.position.x < -_maxXVal)
        {
            transform.position = new Vector3(_maxXVal, transform.position.y, 0f);
        }
    }

    private void CalculateMovement()
    {
        _horizontalInput = Input.GetAxis(_horizontalAxis); // assing a float value from the input manager which returns either 0 or 1
        _verticalInput = Input.GetAxis(_verticalAxis);
        transform.Translate(Vector3.right * _horizontalInput * _playerSpeed * Time.deltaTime); // new Vector3(1,0,0)* -1 * 5f * real time 
        transform.Translate(Vector3.up * _verticalInput * _playerSpeed * Time.deltaTime);
    }
    private void DamageViewChecker()
    {
        if (_lives == _twoLives)
        {
            _rightEngineDamaged.gameObject.SetActive(true);
        }
        else if(_lives == _oneLive)
        {
            _leftEngineDamaged.gameObject.SetActive(true);
        }
    }
    public void Damage() // take one life
    {
        if (_isShieldActive == true)
        {
            _isShieldActive = false;
            _shieldEffect.gameObject.SetActive(false);
            return;
        }
        _lives--;

        DamageViewChecker();

        _uiManager.UpdateLives(_lives);
    }
    private void DeathCheck()
    {
        if (_lives == 0)
        {
            _spawnManager.OnPlayerDeath();
            Destroy(this.gameObject);
            _uiManager.UpdateLives(_lives);
            _uiManager.OnPlayerDeath();
        }
    }
    public void ScoreIncrease()
    {
        _uiManager.IncreaseScore(_pointsForKll);
    }

    private void Start()
    {
        _normalSpeed = _playerSpeed; // assing playerSpeed to normal speed 
        _isShieldActive = true; // has to be active on start
        transform.position = _zeroPos;
    }
    private void Update()
    {
        CalculateMovement();
        PositionRestrain();
        DeathCheck();
    }
}
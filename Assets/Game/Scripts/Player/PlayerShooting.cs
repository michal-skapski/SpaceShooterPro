using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    [SerializeField] private GameObject _laserPrefab = null;
    [SerializeField] private GameObject _tripleShootPrefab = null;
    [SerializeField] private Vector3 _laserStartOffSet = new Vector3(0f, 1.12f, 0f);
    [SerializeField] private float _fireRate = 0.5f;
    [SerializeField] private float _trippleShootActiveTime = 5f;
    [SerializeField] private bool _isTripleShootActive = false;

    private AudioSource _shootSound;

    private float _canFire = -1.0f;

    private IEnumerator _trippleShootDown;



    private void Shooting()
    {
        if (Input.GetKeyDown(KeyCode.Space) && Time.time > _canFire) // transform.position + vector3(0f,0.8f,0f)
        {
            // time.time - how much time has been launch
            _canFire = Time.time + _fireRate; // _canFire = how much time has passed + fire rate // -1,0 = 1x + 0,5 where x = game time in seconds
            // if tripleshotactive is true 
            // fire 3 lasers
            if (_isTripleShootActive == true)
            {
                Instantiate(_tripleShootPrefab, transform.position, Quaternion.identity);
            }
            else 
            {
                Instantiate(_laserPrefab, transform.position + _laserStartOffSet, Quaternion.identity); // intantiate prefab on player pos with no rotation
            }
            _shootSound.Play();
        }
    }
    public void OnTripleBoostCollected()
    {
        _isTripleShootActive = true;
        // start down the power down coroutine for tripple shoot
        _trippleShootDown = TrippleShootPowerDown();
        StartCoroutine(_trippleShootDown);
    }
    private IEnumerator TrippleShootPowerDown()
    {
        yield return new WaitForSeconds(_trippleShootActiveTime);
        _isTripleShootActive = false;
    }
    private void Start()
    {
        _shootSound = this.gameObject.GetComponent<AudioSource>();
    }
    private void Update()
    {
        Shooting();
    }
}

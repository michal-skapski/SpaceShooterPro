using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUps : MonoBehaviour
{
    [SerializeField] private float _speed = 3f;


    [SerializeField] private int _powerUpID = 0; // ID for Power Ups where 0 = Triple Shoot, 1 = Speed, 2 = Shield

    [SerializeField] private AudioClip _pickUpSoundEffect;

    private float _maxYPos = -6f;
    private void PosRestrain()
    {
        if (transform.position.y <= _maxYPos)
        {
            Destroy(this.gameObject); // when leave screen - destroy
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<PlayerShooting>())
        {
            AudioSource.PlayClipAtPoint(_pickUpSoundEffect, transform.position);
            switch (_powerUpID)
            {
                case 0:
                    PlayerShooting playerShooting = other.GetComponent<PlayerShooting>();
                    playerShooting.OnTripleBoostCollected();
                    break;
                case 1:
                    PlayerMovement playerMovement = other.GetComponent<PlayerMovement>();
                    playerMovement.OnSpeedBoostCollected();
                    break;
                case 2:
                    PlayerMovement playerMovement1 = other.GetComponent<PlayerMovement>();
                    playerMovement1.OnShieldCollected();

                    break;             
            }
            Destroy(this.gameObject); // on collected - destroy 
        }
    }
    private void Update()
    {
        transform.Translate(Vector3.down * _speed * Time.deltaTime);
        PosRestrain();
    }
}
